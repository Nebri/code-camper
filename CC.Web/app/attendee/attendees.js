﻿(function () {
    'use strict';

    var controllerId = 'attendees';
    angular
        .module('app')
        .controller('attendees', attendees);

    attendees.$inject = ['common', 'config', 'datacontext'];

    function attendees(common, config, datacontext) {
        /* jshint validthis:true */
        var vm = this;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var keyCodes = config.keyCodes;

        vm.attendees = [];
        vm.attendeeCount = 0;
        vm.attendeeFilteredCount = 0;
        vm.attendeeSearch = '';
        vm.filteredAttendees = [];
        vm.pageChanged = pageChanged;
        vm.paging = {
            currentPage: 1,
            maxPagesToShow: 5,
            pageSize: 15
        };
        vm.refresh = refresh;
        vm.search = search;
        vm.title = 'Attendees';

        Object.defineProperty(vm.paging, 'pageCount', {
            get: function () {
                return Math.floor(vm.attendeeFilteredCount / vm.paging.pageSize) + 1;
            }
        });

        activate();

        function activate() {
            var promises = [getAttendees()];
            common.activateController(promises, controllerId)
                .then(function () { log('Activated Attendees View'); });
        }

        function getAttendeeCount() {                        
            return datacontext.attendee.getCount().then(function (data) {
                return vm.attendeeCount = data;
            });
        }

        function getAttendeeFilteredCount() {            
            vm.attendeeFilteredCount = datacontext.attendee.getFilteredCount(vm.attendeeSearch);
        }

        function getAttendees(forceRefresh) {            
            return datacontext.attendee.getAll(forceRefresh,
                vm.paging.currentPage, vm.paging.pageSize, vm.attendeeSearch)
                .then(
                    function (data) {
                        vm.attendees = data;
                        getAttendeeFilteredCount();
                        if (!vm.attendeeCount || forceRefresh) {
                            getAttendeeCount();
                        }                        
                        return data;
                    }
                );
        }

        function pageChanged(page) {            
            getAttendees();
        }

        function refresh() { getAttendees(true); }

        function search($event) {
            if ($event.keyCodes === keyCodes.esc) {
                vm.attendeeSearch = '';
            }
            getAttendees();
        }

    }
})();
