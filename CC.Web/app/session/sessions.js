﻿(function () {
    'use strict';

    var controllerId = 'sessions';
    angular
        .module('app')
        .controller('sessions', sessions);

    sessions.$inject = ['$location', '$routeParams', 'common', 'config', 'datacontext']; 

    function sessions($location, $routeParams, common, config, datacontext) {
        /* jshint validthis:true */
        var vm = this;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);
        var keyCodes = config.keyCodes;

        var applyFilter = function() { };

        //Bindable properties and functions on vm.
        vm.filteredSessions = [];
        vm.gotoSession = gotoSession;
        vm.sessionsSearch = $routeParams.search || '';
        vm.sessionsFilter = sessionsFilter;
        vm.search = search;
        vm.sessions = [];
        vm.refresh = refresh;
        vm.title = 'Sessions';

        activate();

        function activate() {            
            var promises = [getSessions()];
            common.activateController(promises, controllerId)
                .then(function () {

                    // createSearchThrottle uses values by convention, via it's parameters:
                    //      vm.sessionsSearch is where the user enters the search
                    //      vm.sessions is the original unfiltered array
                    //      vm.filteredSessions is the filtered array
                    //      vm.sessionsFilter is the filtering function
                    applyFilter = common.createSearchThrottle(vm, 'sessions')
                    if (vm.sessionsSearch) { applyFilter(true);}
                    log('Activated Sessions View');
                });
        }

        function getSessions(forceRefresh) {
            return datacontext.session.getPartials(forceRefresh).then(function (data) {
                return vm.sessions = vm.filteredSessions = data;
            });
        }

        function gotoSession(session) {
            if (session && session.id) {
                $location.path('/session/' + session.id);
            }
        }

        function refresh() { getSessions(true); }

        function search($event) {
            if ($event.keyCode == keyCodes.esc) {
                vm.sessionsSearch = '';
                applyFilter(true); // without throttle
            } else {
                applyFilter(); // with throttle
            }            
        }

        function sessionsFilter(session) {
            var textContains = common.textContains;
            var searchText = vm.sessionsSearch;
            var isMatch = searchText ?
                textContains(session.title, searchText)
                || textContains(session.tagsFormatted, searchText)
                || textContains(session.room.name, searchText)
                || textContains(session.track.name, searchText)
                || textContains(session.speaker.fullName, searchText)
                : true;
            return isMatch;
        }

    }
})();
