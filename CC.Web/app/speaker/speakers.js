﻿(function () {
    'use strict';

    var controllerId = 'speakers';
    angular
        .module('app')
        .controller('speakers', speakers);

    speakers.$inject = ['$location', 'common', 'config', 'datacontext']; 

    function speakers($location, common, config, datacontext) {
        /* jshint validthis:true */
        var vm = this;
        var keyCodes = config.keyCodes;
        var getLogFn = common.logger.getLogFn;
        var log = getLogFn(controllerId);

        // bindable properties and functions
        vm.filteredSpeakers = [];
        vm.gotoSpeaker = gotoSpeaker;
        vm.search = search;
        vm.speakersSearch = '';
        vm.speakers = [];
        vm.title = 'Speakers';
        vm.refresh = refresh;

        activate();

        function activate() {
            var promises = [getSpeakers()];
            common.activateController(promises, controllerId)
                .then(function () { log('Activated Speakers View'); });            
        }

        function getSpeakers(forceRefresh) {
            return datacontext.speaker.getPartials(forceRefresh).then(function (data) {
                vm.speakers = data;
                applyFilter();
                return vm.speakers;
            });
        }

        function gotoSpeaker(speaker) {
            if (speaker && speaker.id) {
                $location.path('/speaker/' + speaker.id);
            }
        }

        function refresh() { getSpeakers(true); }

        function search($event) {
            if ($event.keyCode == keyCodes.esc) {
                vm.speakersSearch = '';
            }
            applyFilter();
        }

        function applyFilter() {
            vm.filteredSpeakers = vm.speakers.filter(speakerFilter);
        }

        function speakerFilter(speaker) {
            var isMatch = vm.speakerSearch
                ? common.textContains(speaker.fullName, vm.speakerSearch)
                : true;
            return isMatch;
        }

    }
})();
